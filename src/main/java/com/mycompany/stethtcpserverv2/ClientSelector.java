/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.stethtcpserverv2;

import static com.mycompany.stethtcpserverv2.TCPServer.CSockets;
import static com.mycompany.stethtcpserverv2.TCPServer.Cis;
import static com.mycompany.stethtcpserverv2.TCPServer.Cos;
import static com.mycompany.stethtcpserverv2.TCPServer.ESockets;
import static com.mycompany.stethtcpserverv2.TCPServer.Eis;
import static com.mycompany.stethtcpserverv2.TCPServer.Eos;
import static com.mycompany.stethtcpserverv2.TCPServer.doctor;
import static com.mycompany.stethtcpserverv2.TCPServer.patient;
import static com.mycompany.stethtcpserverv2.TCPServer.status;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 *
 * @author jideesh
 */
public class ClientSelector {

    public void selector(Socket connectionsocket) {
        try {
            InputStream is = connectionsocket.getInputStream();
            OutputStream os = connectionsocket.getOutputStream();
            System.out.println("connection accepted");
            byte[] temp = new byte[2];
            Thread t = new Thread() {
                public void run() {
                    try {
                        while (is.read(temp) > 0) {
                            if (temp[0] == 'C' && temp[1] == '0') {
                                for (int i = 0; i < 10; i++) {
                                    if (!status[i][0]) {
                                        CSockets[i] = connectionsocket;
                                        patient[i] = new CThread();
                                        patient[i].threadnum = i;
                                        patient[i].is = Cis[i] = is;
                                        patient[i].os = Cos[i] = os;
                                        status[i][0] = true;
                                        patient[i].start();
                                        break;

                                    } else if (i == 9) {
                                        System.out.println("No avalable threads");
                                        byte[] temporary = new byte[3];
                                        temporary[0] = 'E';
                                        temporary[0] = 'R';
                                        temporary[0] = 'R';
                                        connectionsocket.getOutputStream().write(temporary);
                                        connectionsocket.close();

                                    }
                                }
                                break;

                            }else if(temp[0]=='E' && temp[1]=='0')
                            {
                                for(int i=0;i<10;i++)
                                {
                                    if(!status[i][1])
                                    {
                                        ESockets[i]=connectionsocket;
                                        System.out.println(i);
                                        doctor[i] = new EThread();
                                        doctor[i].threadnum=i;
                                        doctor[i].is = Eis[i] = is;
                                        doctor[i].os = Eos[i] = os;
                                        status[i][1]=true;
                                        doctor[i].start();
                                        break;
                                    }
                                    else if(i==9)
                                    {
                                        System.out.println("No avalable threads");
                                        byte[] temporary = new byte [3];
                                        temporary[0]='E';temporary[0]='R';temporary[0]='R';
                                        connectionsocket.getOutputStream().write(temporary);
                                        connectionsocket.close();
                                    }
                                }
                                break;
                                
                            }

                        }

                    } catch (IOException e) {

                    }
                }
            };
            t.start();
        } catch (IOException e) {

        }
    }

}
