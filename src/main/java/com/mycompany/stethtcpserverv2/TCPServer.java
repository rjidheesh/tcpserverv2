/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.stethtcpserverv2;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import static java.lang.System.exit;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author jideesh
 */
public class TCPServer {

    public static ServerSocket socket;
    public static Socket connectionsocket;
    public static boolean status[][] = new boolean[10][2];
    public static boolean[] dstatus = new boolean[10];
    public static Socket[] CSockets = new Socket[10];
    public static Socket[] ESockets = new Socket[10];
    public static CThread patient[] = new CThread[10];
    public static EThread doctor[] = new EThread[10];
    public static InputStream[] Cis = new InputStream[10];
    public static OutputStream[] Cos = new OutputStream[10];
    
     public static InputStream[] Eis = new InputStream[10];
     public static OutputStream[] Eos = new OutputStream[10];

    public static void main(String[] args) {

        try {
            socket = new ServerSocket(1237);    // New Socket will be 1237
            System.out.println("Server started at 1237");

            for (int i = 0; i < 10; i++) {
                status[i][1] = false;
                status[i][0] = false;
                dstatus[i] = false;
            }
            while (true) {
                System.out.println("Waiting for connection");
                connectionsocket = socket.accept();
                ClientSelector obj = new ClientSelector();
                obj.selector(connectionsocket);
            }

        } catch (IOException ex) {
            System.out.println("Cant open... Port is used by another program");
            exit(0);
        }

    }

}
